#!/bin/bash
set -m


if [ -f /app/site.nginx ]; then
	echo "*** nginx configuration ***"
	rm /etc/nginx/sites-available/default
	ln -s /app/site.nginx /etc/nginx/sites-available/default
	service nginx restart
else
	echo "site.nginx configuration not found!"
fi

if [ -f /app/requirements.txt ]; then
        echo "*** requirements ***"
        pip install -r /app/requirements.txt
else
        echo "requirements.txt not found!"
fi

if [ -f /app/manage.py ]; then
	echo "*** collectstatic ***"
	cd /app && python manage.py collectstatic --noinput
else
        echo "manage.py not found!"
fi

if [ -f /app/Procfile ]; then
	echo "*** foreman start ***"
	cd /app && foreman start
else
	echo "Procfile not found!"
fi
